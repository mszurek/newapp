class CreateBeers < ActiveRecord::Migration[5.1]
  def change
    create_table :beers do |t|
      t.string :name
      t.float :vol
      t.integer :beer_type_id
      t.timestamps
    end
  end
end
