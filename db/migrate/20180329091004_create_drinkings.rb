class CreateDrinkings < ActiveRecord::Migration[5.1]
  def change
    create_table :drinkings do |t|
      t.integer :person_id
      t.integer :beer_id
      t.timestamps
    end
  end
end
