class BeersController < ApplicationController

  before_action :prepare_record, only: [:show, :edit, :update]

  def prepare_record
      @t=BeerType.all
      @b=Beer.includes(:beer_type).find(params[:id])
  end  


  def show
    ##prepare_record

    #render action: 'page'   #app/views/beers/page.html.erb
    #defaultowo to bedzie  render  action: 'show'
    #lub po prostu: render 'page'  

    ###redirect_to(edit_beer_path) 

    ###redirect_to (home_path)

    respond_to do |format|
       format.html
       format.json { render json: @b }
       format.xml { render xml: @b }     #nie dziala, bo czegos tu brakuje
    end 
  end

  def edit
    ##prepare_record
  end  

  def update
     ##@b = Beer.find(params[:id])
     if @b.update(beer_params) 
        redirect_to (beers_path)    #moze byc tez  :beers
     else
        ##@t=BeerType.all
        flash[:notice] = "Jest źle"
        render 'edit'

        ###redirect_to (edit_beer_path) 

     end
  end

  #------------------------------------------

  def index
    if params[:typ]
     @t=BeerType.where(name: params[:typ]).first
     if @t.nil?
      @b=[]
     else
      @b=@t.beers
     end 
    else  
     @b=Beer.includes(:beer_type).all.order(name: :asc)
    end 
  end


  def destroy
    #usuwanie 
  end


  private
    def beer_params
       params.require(:beer).permit(:name, :voltarz, :beer_type_id) 
    end  

end
