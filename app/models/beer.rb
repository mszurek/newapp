class Beer < ApplicationRecord

   
   validates :name, 
              presence: true,
              length: {minimum: 3}
   
   ## tak tez mozna validates_length_of :name, minimum: 3


   belongs_to :beer_type, :class_name => "BeerType"
   has_many :drinkings


   def voltarz
      if vol.nil?
         return ""
      else
         return (vol*100).to_s.slice(0..3)+"%"
      end
   end 

   


   def voltarz=(value)
   	  self.vol = value.to_f/100.0
   end



   def high_voltarz?
   	 vol > 0.16
   end

   def high_voltarz!
   	 vol > 0.16
   	 save
   end


end
