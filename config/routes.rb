Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :beers
  get 'home', to: 'application#home'
  get 'beers/type/:typ' => 'beers#index', as: 'beers_of_type'
  resources :drinkings
  resources :people
  get 'drinkings/new/:person' => 'drinkings#new', as: 'drinkings_newp'
  get 'drinkings/new/:beer' => 'drinkings#new', as: 'drinkings_newb'
end
